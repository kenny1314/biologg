package com.example.biologg.twoLvl;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.biologg.BiologMainActivity;
import com.example.biologg.DatabaseHelper;
import com.example.biologg.R;
import com.example.biologg.threeLvl.ChangeAnimActivity;
import com.example.biologg.threeLvl.DeleteAnimActivity;
import com.example.biologg.threeLvl.InfoAnimActivity;

import java.util.ArrayList;

public class SearchEditAnimActivity extends AppCompatActivity {

    ListView userList;
    DatabaseHelper databaseHelper;
    SQLiteDatabase db;
    Cursor cursor;
    SimpleCursorAdapter userAdapter;
    EditText userFilter;
    Spinner spinner;
    String selectSpinnerStr;
    long id = 0;


    String[] biostr = new String[6];
    ArrayList<String> phones = new ArrayList();
    long userId = 0;
    ArrayAdapter<String> adapterTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_edit_anim);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        userList = (ListView) findViewById(R.id.listBioSearchSe);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getLong("id");
            System.out.println("edit " + userId);

        } else {
            System.out.println("bundle НЕТУ edit");
        }

        userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = null;
                if (userId == -9) {
//                    Toast toast = Toast.makeText(getApplicationContext(), "-9999", Toast.LENGTH_SHORT);
//                    toast.show();
                    intent = new Intent(getApplicationContext(), DeleteAnimActivity.class);
                    intent.putExtra("id", id);
                    startActivity(intent);
                }
                if (userId == 1) {

                    Intent intent2 = new Intent(getApplicationContext(), ChangeAnimActivity.class);
                    intent2.putExtra("id", id);
                    startActivity(intent2);

                }
                if (userId == 2) {
                    Intent intent3 = new Intent(getApplicationContext(), InfoAnimActivity.class);
                    intent3.putExtra("id", id);
                    startActivity(intent3);

                }

//                Toast toast = Toast.makeText(getApplicationContext(), "Вы нажали anim", Toast.LENGTH_SHORT);
//                toast.show();

            }
        });


        userFilter = (EditText) findViewById(R.id.userFilterSe);

        spinner = (Spinner) findViewById(R.id.spinnerSe);

//колонки для spinner bз serach
        biostr[0] = DatabaseHelper.COLUMN_IDENTB;
        biostr[1] = DatabaseHelper.COLUMN_GENDERB;
        biostr[2] = DatabaseHelper.COLUMN_AGEB;
        biostr[3] = DatabaseHelper.COLUMN_CLASSB;
        biostr[4] = DatabaseHelper.COLUMN_INOC;
        biostr[5] = DatabaseHelper.COLUMN_CUBSQ;

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, biostr);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);


        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectSpinnerStr = (String) parent.getItemAtPosition(position);
                Toast toast = Toast.makeText(getApplicationContext(), selectSpinnerStr, Toast.LENGTH_SHORT);
                toast.show();
                //что-то новое

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        spinner.setOnItemSelectedListener(itemSelectedListener);


        //здесь создаётся база данных
        databaseHelper = new DatabaseHelper(getApplicationContext());

        //создаём базу данных
        databaseHelper.create_db();

    }

    public void onResume() {
        super.onResume();

        db = databaseHelper.open();


        //получаем данные в виде курсора
        cursor = db.rawQuery("select _id,_id|| ' ident: ' || ident || ' age: ' || age|| ' gender: ' ||gender as ftvName," +
                "'class: '||classb|| ' inoc: '|| inoc|| ' cubsq: '||cubsq as ftvName2,gender,ident,age,classb,inoc,cubsq from " + DatabaseHelper.TABLE2, null);

        String[] headers = new String[]{"ftvName", "ftvName2"};

        userAdapter = new SimpleCursorAdapter(this, android.R.layout.two_line_list_item,
                cursor, headers, new int[]{android.R.id.text1, android.R.id.text2}, 0);


        if (!userFilter.getText().toString().isEmpty())
            userAdapter.getFilter().filter(userFilter.getText().toString());

        //слушатель для EditText
        userFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            //фильтровать данные в simple cursor adapter
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //если ничего не введено, то выводим все записи
        //если есть данные, то фильтруем по like
        userAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                if (constraint == null || constraint.length() == 0) {

                    return db.rawQuery("select _id,_id|| ' ident: ' || ident || ' age: ' || age|| ' gender: ' ||gender as ftvName," +
                            "'class: '||classb|| ' inoc: '|| inoc|| ' cubsq: '||cubsq as ftvName2,gender,ident,age,classb,inoc,cubsq from " + DatabaseHelper.TABLE2, null);
                } else {

                    //выборка по spinner
                    if (selectSpinnerStr.equals("ident")) {
                        //здесь сделать обработку radio button для выбора поля для поиска

                        return db.rawQuery("select _id,_id|| ' ident: ' || ident || ' age: ' || age|| ' gender: ' ||gender as ftvName," +
                                "'class: '||classb|| ' inoc: '|| inoc|| ' cubsq: '||cubsq as ftvName2,gender,ident,age,classb,inoc,cubsq from " + DatabaseHelper.TABLE2 + " where " +
                                DatabaseHelper.COLUMN_IDENTB + " like ?", new String[]{"%" + constraint.toString() + "%"});
                    }
                    if (selectSpinnerStr.equals("gender")) {
                        return db.rawQuery("select _id,_id|| ' ident: ' || ident || ' age: ' || age|| ' gender: ' ||gender as ftvName," +
                                "'class: '||classb|| ' inoc: '|| inoc|| ' cubsq: '||cubsq as ftvName2,gender,ident,age,classb,inoc,cubsq from " + DatabaseHelper.TABLE2 + " where " +
                                DatabaseHelper.COLUMN_GENDERB + " like ?", new String[]{"%" + constraint.toString() + "%"});
                    }
                    if (selectSpinnerStr.equals("age")) {
                        return db.rawQuery("select _id,_id|| ' ident: ' || ident || ' age: ' || age|| ' gender: ' ||gender as ftvName," +
                                "'class: '||classb|| ' inoc: '|| inoc|| ' cubsq: '||cubsq as ftvName2,gender,ident,age,classb,inoc,cubsq from " + DatabaseHelper.TABLE2 + " where " +
                                DatabaseHelper.COLUMN_AGEB + " like ?", new String[]{"%" + constraint.toString() + "%"});

                    }
                    if (selectSpinnerStr.equals("classb")) {
                        return db.rawQuery("select _id,_id|| ' ident: ' || ident || ' age: ' || age|| ' gender: ' ||gender as ftvName," +
                                "'class: '||classb|| ' inoc: '|| inoc|| ' cubsq: '||cubsq as ftvName2,gender,ident,age,classb,inoc,cubsq from " + DatabaseHelper.TABLE2 + " where " +
                                DatabaseHelper.COLUMN_CLASSB + " like ?", new String[]{"%" + constraint.toString() + "%"});
                    }
                    if (selectSpinnerStr.equals("inoc")) {
                        return db.rawQuery("select _id,_id|| ' ident: ' || ident || ' age: ' || age|| ' gender: ' ||gender as ftvName," +
                                "'class: '||classb|| ' inoc: '|| inoc|| ' cubsq: '||cubsq as ftvName2,gender,ident,age,classb,inoc,cubsq from " + DatabaseHelper.TABLE2 + " where " +
                                DatabaseHelper.COLUMN_INOC + " like ?", new String[]{"%" + constraint.toString() + "%"});
                    }
                    if (selectSpinnerStr.equals("cubsq")) {
                        return db.rawQuery("select _id,_id|| ' ident: ' || ident || ' age: ' || age|| ' gender: ' ||gender as ftvName," +
                                "'class: '||classb|| ' inoc: '|| inoc|| ' cubsq: '||cubsq as ftvName2,gender,ident,age,classb,inoc,cubsq from " + DatabaseHelper.TABLE2 + " where " +
                                DatabaseHelper.COLUMN_CUBSQ + " like ?", new String[]{"%" + constraint.toString() + "%"});
                    }
                }
                return null;
            }
        });

        userList.setAdapter(userAdapter);

    }
    public  void onBackPressed(){

        long idd=2;
        Intent intent = new Intent(this, BiologMainActivity.class);
        intent.putExtra("id", idd);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);

    }

    public void onDestroy() {
        super.onDestroy();

        Toast.makeText(this, "", Toast.LENGTH_SHORT).show();


        db.close();
        cursor.close();
    }
}

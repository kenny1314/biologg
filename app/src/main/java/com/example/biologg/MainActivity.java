package com.example.biologg;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.biologg.threeLvl.DeleteBioActivity;
import com.example.biologg.twoLvl.SearchEditAnimActivity;
import com.example.biologg.twoLvl.SearchEditBioActivity;

public class MainActivity extends AppCompatActivity {

    ListView userList;
    DatabaseHelper databaseHelper;
    SQLiteDatabase db;
    Cursor cursor;
    SimpleCursorAdapter userAdapter;
    //EditText userFilter;
    TextView viewShowText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //чтобы не появлялась клавиатуры
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        userList = (ListView) findViewById(R.id.list);

//        viewShowText = (TextView) findViewById(R.id.showText);


        //здесь создаётся база данных
        databaseHelper = new DatabaseHelper(getApplicationContext());

        //создаём базу данных
        databaseHelper.create_db();
    }


    public void onResume() {
        super.onResume();

        db = databaseHelper.open();
//
//        //получаем данные в виде курсора
//        cursor = db.rawQuery("select * from " + DatabaseHelper.TABLE, null);
//        String[] headers = new String[]{DatabaseHelper.COLUMN_NAME, DatabaseHelper.COLUMN_YEAR};
//
//
//        userAdapter = new SimpleCursorAdapter(this, android.R.layout.two_line_list_item,
//                cursor, headers, new int[]{android.R.id.text1, android.R.id.text2}, 0);
//
//        userList.setAdapter(userAdapter);
    }

    public void onClickEnter(View view) {

        EditText editLogin = (EditText) findViewById(R.id.editLogin);
        EditText editPass = (EditText) findViewById(R.id.editPass);
        editLogin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        editPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);


//        TextView textLogin = (TextView) findViewById(R.id.textShowLogin);
//        TextView textPassword = (TextView) findViewById(R.id.textShowPassword);


        String login;
        String password;


        //если это админ, то выбрать с кем работать
        //если биолог то сразу выбор действия
        //админ не хранится в базе

        System.out.println("\tlog: "+editLogin.getText().toString().equals("admin"));
        System.out.println("\tpass: "+editPass.equals("admin"));

        System.out.println("_l: "+editLogin.getText().toString()+" : "+"admin"+" : "+editLogin.getText().length()+" : "+ "admin".length());
        System.out.println("_p: "+editPass.getText());


        boolean tr2=false;

        if (editLogin.getText().toString().equals("admin") && editPass.getText().toString().equals("admin")) {
            tr2=true;
//            System.out.println("log: "+textLogin.equals("admin"));
//            System.out.println("pass: "+textPassword.equals("admin"));
            Intent intent = new Intent(this, ChoseBioAnimActivity.class);
            startActivity(intent);
        }


        cursor = db.rawQuery("select * from " + DatabaseHelper.TABLE + " where "
                + DatabaseHelper.COLUMN_LOGINZ + "=?", new String[]{editLogin.getText().toString()});

        //это вход
        if (cursor.moveToFirst()) {
            do {
                Password.count++;
                login = cursor.getString(1);
                password = cursor.getString(4);
//
//                viewShowText.setText(login + " : " + password);
//
                if (login.equals(editLogin.getText().toString()) && password.equals(editPass.getText().toString())) {
//
                    Toast toast = Toast.makeText(this, "Правильный логин и пароль", Toast.LENGTH_SHORT);
                    toast.show();
                    long idd=2;
                    Intent intent = new Intent(this, BiologMainActivity.class);
                    intent.putExtra("id",idd);
                    startActivity(intent);
                } else {
                    Toast toast = Toast.makeText(this, "Неверный логин или пароль", Toast.LENGTH_SHORT);
                    toast.show();
                }


            }
            while (cursor.moveToNext());
        } else {
            if(!tr2) {
                Toast toast = Toast.makeText(this, "Таких данных нет", Toast.LENGTH_SHORT);
                toast.show();
            }

        }


        editPass.setText("");
        editPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);



//        Intent intent = new Intent(this, BiologMainActivity.class);
//        Intent intent = new Intent(this, ChoseBioAnimActivity.class);
//        startActivity(intent);

//        textLogin.setText(editLogin.getText());
//        textPassword.setText(editPass.getText());

//        Toast toast = Toast.makeText(this, "Count: " + Password.count, Toast.LENGTH_SHORT);
//        toast.show();


    }

    public void onDestroy() {
        super.onDestroy();
//
//        db.close();
//        cursor.close();
    }

    //заменяем на инфо
    public void onClickNow(View view) {
//        Intent intent = new Intent(getApplicationContext(), InfoBioActivity.class);
//        long idd=2;
//        intent.putExtra("id",idd);
//        startActivity(intent);

//        Intent intent = new Intent(getApplicationContext(), ChangeAnimActivity.class);
//        long idd=2;
//        intent.putExtra("id",idd);
//        startActivity(intent);
//        Intent intent = new Intent(getApplicationContext(), InfoBioActivity.class);
        Intent intent = new Intent(getApplicationContext(), DeleteBioActivity.class);
        long idd = 2;
        intent.putExtra("id", idd);
        startActivity(intent);
    }


    public void onClickAdd(View view) {
//        Intent intent = new Intent(this, EditAnimActivity.class);
//        startActivity(intent);


//        Intent intent = new Intent(getApplicationContext(), InfoBioActivity.class);
//        long idd=2;
//        intent.putExtra("id",idd);
//        startActivity(intent);
//        Intent intent = new Intent(getApplicationContext(), SearchEditAnimActivity.class);
//        long idd=2;
//        intent.putExtra("id",idd);
//        startActivity(intent);
        Intent intent = new Intent(getApplicationContext(), SearchEditBioActivity.class);
        long idd = 2;
        intent.putExtra("id", idd);
        startActivity(intent);
    }
}

package com.example.biologg.threeLvl;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.biologg.BiologMainActivity;
import com.example.biologg.DatabaseHelper;
import com.example.biologg.R;
import com.example.biologg.twoLvl.SearchEditAnimActivity;
import com.example.biologg.twoLvl.SearchEditBioActivity;
import com.example.biologg.z_trash.EditAnimActivity;

public class ChangeAnimActivity extends AppCompatActivity {

    EditText editIdent;
    EditText editGender;
    EditText editAge;
    EditText editClassb;
    EditText editInoc;
    EditText editCubsq;

    DatabaseHelper sqlHelper;
    SQLiteDatabase db;
    Cursor cursor;

    long userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_anim);


        editIdent = (EditText) findViewById(R.id.editNameAnim);
        editGender = (EditText) findViewById(R.id.editGenderAnim);
        editAge = (EditText) findViewById(R.id.editAgeAnim);
        editClassb = (EditText) findViewById(R.id.editClassbAnim);
        editInoc = (EditText) findViewById(R.id.editInocAnim);
        editCubsq = (EditText) findViewById(R.id.editCubsQAnim);


        sqlHelper = new DatabaseHelper(this);
        db = sqlHelper.open();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getLong("id");
            System.out.println("change " + userId);
        }


        //если редактирование, то получаем данные с выбранным id и выводим в editbox
        if (userId > 0) {

            cursor = db.rawQuery("select * from " + DatabaseHelper.TABLE2 + " where "
                    + DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(userId)});

//            cursor = db.rawQuery("select _id,_id|| ' ident: ' || ident || ' age: ' || age|| ' gender: ' ||gender as ftvName," +
//                    "'class: '||classb|| ' inoc: '|| inoc|| ' cubsq: '||cubsq as ftvName2,ident,gender,age,classb,inoc,cubsq from " + DatabaseHelper.TABLE2, null);

            System.out.println("USER ID : " + userId);
            cursor.moveToFirst();

//            editIdent.setText(cursor.getString(3));
//            editGender.setText(cursor.getString(4));
//            editAge.setText(String.valueOf(cursor.getInt(5)));
//            editClassb.setText(cursor.getString(6));
//            editInoc.setText(cursor.getString(7));
//            editCubsq.setText(String.valueOf(cursor.getInt(8)));
//
            editIdent.setText(cursor.getString(1));
            editGender.setText(cursor.getString(2));
            editAge.setText(String.valueOf(cursor.getInt(3)));
            editClassb.setText(cursor.getString(4));
            editInoc.setText(cursor.getString(5));
            editCubsq.setText(String.valueOf(cursor.getInt(6)));
        }


    }


    //подключить сохранения
    public void onClickAddAnim(View view) {

        boolean tr = true;
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_IDENTB, editIdent.getText().toString());
        cv.put(DatabaseHelper.COLUMN_GENDERB, editGender.getText().toString());
        int nAge;
        int nSubsq;
        try {
            nAge = Integer.parseInt(editAge.getText().toString());
        } catch (NumberFormatException ex) {
            Toast toast = Toast.makeText(getApplicationContext(), "Неверный формат Age", Toast.LENGTH_SHORT);
            toast.show();
            nAge = -888;
            tr=false;
        }

        try {
            nSubsq = Integer.parseInt(editCubsq.getText().toString());
        } catch (NumberFormatException ex) {
            Toast toast = Toast.makeText(getApplicationContext(), "Неверный формат Cubsq", Toast.LENGTH_SHORT);
            toast.show();
            nSubsq = -777;
            tr=false;
        }

        if(tr) {

            cv.put(DatabaseHelper.COLUMN_AGEB, nAge);
//        cv.put(DatabaseHelper.COLUMN_AGEB, Integer.parseInt(editAge.getText().toString()));
            cv.put(DatabaseHelper.COLUMN_CLASSB, editClassb.getText().toString());
            cv.put(DatabaseHelper.COLUMN_INOC, editInoc.getText().toString());
//        cv.put(DatabaseHelper.COLUMN_CUBSQ, Integer.parseInt(editCubsq.getText().toString()));
            cv.put(DatabaseHelper.COLUMN_CUBSQ, nSubsq);
            //если новая запись, то добавляем, если старая то обновляем
            if (userId > 0) {
                db.update(DatabaseHelper.TABLE2, cv, DatabaseHelper.COLUMN_ID + "=" + String.valueOf(userId), null);
            } else {
                db.insert(DatabaseHelper.TABLE2, null, cv);

            }

            Toast toast = Toast.makeText(getApplicationContext(), "ДОБАВЛЕНО", Toast.LENGTH_SHORT);
            toast.show();
            goHome();
        }

    }

    private void goHome() {

        //если добавление, то на просмотр
        //если редактирование, то на редактирование


        long idd = 0;
        if (userId == 0) {
            idd = 2;
        }
        if (userId > 0) {
            idd = 1;
        }

        Intent intent = new Intent(this, SearchEditAnimActivity.class);
        intent.putExtra("id", idd);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    public  void onBackPressed(){

        long idd = 0;
        if (userId == 0) {
            idd = 2;
        }
        if (userId > 0) {
            idd = 1;
        }

        //1 ред 2 просмотр
        Intent intent = new Intent(this, SearchEditAnimActivity.class);
        intent.putExtra("id", idd);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);

    }


}

package com.example.biologg.threeLvl;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.biologg.DatabaseHelper;
import com.example.biologg.MainActivity;
import com.example.biologg.R;
import com.example.biologg.twoLvl.SearchEditBioActivity;

public class DeleteBioActivity extends AppCompatActivity {

    TextView tvID;
    TextView tvLogin;
    TextView tvIdent;
    TextView tvStage;
    TextView tvPassword;
    Cursor cursor;
    SQLiteDatabase db;
    DatabaseHelper databaseHelper;
    long userId = 0;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_bio);

        imageView = (ImageView) findViewById(R.id.imgDelBio);

        tvID = (TextView) findViewById(R.id.tvIDDelBio);
        tvLogin = (TextView) findViewById(R.id.tvLoginDelBio);
        tvIdent = (TextView) findViewById(R.id.tvIdentDelBio);
        tvStage = (TextView) findViewById(R.id.tvStageDelBio);
        tvPassword = (TextView) findViewById(R.id.tvPasswordDelBio);


        //здесь создаётся база данных
        databaseHelper = new DatabaseHelper(getApplicationContext());
        db = databaseHelper.open();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getLong("id");
            System.out.println("Info id:" + userId);
            tvID.append(String.valueOf(userId));
        }


        int idimg=0;
        if (userId > 0) {

            cursor = db.rawQuery("select * from " + DatabaseHelper.TABLE + " where "
                    + DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(userId)});

            System.out.println("USER ID : " + userId);
            cursor.moveToFirst();
            tvLogin.setText(cursor.getString(1));
            tvIdent.setText(cursor.getString(2));
            tvStage.append(String.valueOf(cursor.getInt(3)));
            tvPassword.append(cursor.getString(4));
            idimg=cursor.getInt(5);
        }



        //если idimg то вставить картинку
        String str1 = Long.toString(userId);


        System.out.println("___Idimg: " + idimg);

        if (idimg==1) {
            imageView.setImageResource(R.drawable.b1);
        }
        else if (idimg==2) {
            imageView.setImageResource(R.drawable.b2);
        }
        else if (idimg==3) {
            imageView.setImageResource(R.drawable.b3);
        }
        else if (idimg==4) {
            imageView.setImageResource(R.drawable.b4);
        }
        else {
            imageView.setImageResource(R.drawable.b2);
        }


    }


    public void onClickDeleteBio(View view) {
        db.delete(DatabaseHelper.TABLE, "_id = ?", new String[]{String.valueOf(userId)});
        goHome();
    }

    private void goHome() {
//отправить на поиск записей
        long idd=-9;
        Intent intent = new Intent(this, SearchEditBioActivity.class);
        intent.putExtra("id",idd);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    public  void onBackPressed(){
        Toast toast=Toast.makeText(getApplicationContext(),"onBack",Toast.LENGTH_SHORT);
        toast.show();
        long idd=-9;
        Intent intent = new Intent(this, SearchEditBioActivity.class);
        intent.putExtra("id",idd);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

}

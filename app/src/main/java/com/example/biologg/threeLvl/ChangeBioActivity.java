package com.example.biologg.threeLvl;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.biologg.BiologMainActivity;
import com.example.biologg.DatabaseHelper;
import com.example.biologg.R;
import com.example.biologg.twoLvl.SearchEditBioActivity;
import com.example.biologg.z_trash.EditAnimActivity;

public class ChangeBioActivity extends AppCompatActivity {

    EditText editLogin;
    EditText editIdent;
    EditText editStage;
    EditText editPassword;

    DatabaseHelper sqlHelper;
    SQLiteDatabase db;
    Cursor cursor;
    RadioGroup radioGroup;
    RadioButton r1;
    RadioButton r2;
    RadioButton r3;
    RadioButton r4;

    int newChoseImg = 0;

    long userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_bio);


        editLogin = (EditText) findViewById(R.id.editLoginBio);
        editIdent = (EditText) findViewById(R.id.editIdentBio);
        editStage = (EditText) findViewById(R.id.editStageBio);
        editPassword = (EditText) findViewById(R.id.editPasswordBio);

        r1 = (RadioButton) findViewById(R.id.r1img);
        r2 = (RadioButton) findViewById(R.id.r2img);
        r3 = (RadioButton) findViewById(R.id.r3img);
        r4 = (RadioButton) findViewById(R.id.r4img);

        //дбавить радиокнопки


        radioGroup = (RadioGroup) findViewById(R.id.radios);
        r1.toggle();
        newChoseImg=1;
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Toast toast = null;
                switch (checkedId) {
                    case R.id.r1img:
//                        toast = Toast.makeText(getApplicationContext(), "r1", Toast.LENGTH_SHORT);
//                        toast.show();
                        newChoseImg = 1;
                        break;
                    case R.id.r2img:
//                        toast = Toast.makeText(getApplicationContext(), "r2", Toast.LENGTH_SHORT);
//                        toast.show();
                        newChoseImg = 2;
                        break;
                    case R.id.r3img:
//                        toast = Toast.makeText(getApplicationContext(), "r3", Toast.LENGTH_SHORT);
//                        toast.show();
                        newChoseImg = 3;
                        break;
                    case R.id.r4img:
//                        toast = Toast.makeText(getApplicationContext(), "r4", Toast.LENGTH_SHORT);
//                        toast.show();
                        newChoseImg = 4;
                        break;
                    default:
                        break;
                }
            }
        });


        sqlHelper = new DatabaseHelper(this);
        db = sqlHelper.open();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getLong("id");
            System.out.println("change " + userId);
        }


        //если редактирование, то получаем данные с выбранным id и выводим в editbox
        if (userId > 0) {

            cursor = db.rawQuery("select * from " + DatabaseHelper.TABLE + " where "
                    + DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(userId)});

            System.out.println("USER ID : " + userId);
            cursor.moveToFirst();
            editLogin.setText(cursor.getString(1));
            editIdent.setText(cursor.getString(2));
            editStage.setText(String.valueOf(cursor.getInt(3)));
            editPassword.setText(cursor.getString(4));

            int choceImg = 0;
            choceImg = cursor.getInt(5);

            switch (choceImg) {
                case 1:
                    r1.toggle();
                    break;
                case 2:
                    r2.toggle();
                    break;
                case 3:
                    r3.toggle();
                    break;
                case 4:
                    r4.toggle();
                    break;
            }

        }


    }


    //подключить сохранения
    public void onClickAddBio(View view) {

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_LOGINZ, editLogin.getText().toString());
        cv.put(DatabaseHelper.COLUMN_IDENTZ, editIdent.getText().toString());
        boolean tr = true;
        int nStage;
        try {
            nStage = Integer.parseInt(editStage.getText().toString());
        } catch (NumberFormatException ex) {
            Toast toast = Toast.makeText(getApplicationContext(), "Неверный формат Stage", Toast.LENGTH_SHORT);
            toast.show();
            nStage = -888;
            tr = false;
        }

        if (tr) {
            cv.put(DatabaseHelper.COLUMN_STAGEZ, nStage);
//        cv.put(DatabaseHelper.COLUMN_STAGEZ, Integer.parseInt(editStage.getText().toString()));
            cv.put(DatabaseHelper.COLUMN_PASSWORDZ, editPassword.getText().toString());
            cv.put(DatabaseHelper.COLUMN_IDIMGZ, newChoseImg);

            //если новая запись, то добавляем, если старая то обновляем
            if (userId > 0) {
                db.update(DatabaseHelper.TABLE, cv, DatabaseHelper.COLUMN_ID + "=" + String.valueOf(userId), null);
            } else {
                db.insert(DatabaseHelper.TABLE, null, cv);

            }

            Toast toast = Toast.makeText(getApplicationContext(), "ДОБАВЛЕНО", Toast.LENGTH_SHORT);
            toast.show();
            goHome();
        }
    }

    private void goHome() {

        long idd = 0;
        if (userId == 0) {
            idd = 2;
        }
        if (userId > 0) {
            idd = 1;
        }
        Intent intent = new Intent(this, SearchEditBioActivity.class);
        intent.putExtra("id", idd);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }



    public  void onBackPressed(){

        long idd = 0;
        if (userId == 0) {
            idd = 2;
        }
        if (userId > 0) {
            idd = 1;
        }

        //1 ред 2 просмотр
        Intent intent = new Intent(this, SearchEditBioActivity.class);
        intent.putExtra("id", idd);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);

    }


}

package com.example.biologg.threeLvl;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.biologg.DatabaseHelper;
import com.example.biologg.R;
import com.example.biologg.twoLvl.SearchEditAnimActivity;
import com.example.biologg.z_trash.EditAnimActivity;

public class DeleteAnimActivity extends AppCompatActivity {

    TextView tvID;

    TextView tvIdent;
    TextView tvGender;
    TextView tvAge;
    TextView tvClassb;
    TextView tvInoc;
    TextView tvCubsq;

    Cursor cursor;
    SQLiteDatabase db;
    DatabaseHelper databaseHelper;
    long userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_anim);

        tvID = (TextView) findViewById(R.id.tvIDDel);
        tvIdent = (TextView) findViewById(R.id.tvIdentDelAnim);
        tvGender = (TextView) findViewById(R.id.tvGenderDelAnim);
        tvAge = (TextView) findViewById(R.id.tvAgeDelAnim);
        tvClassb = (TextView) findViewById(R.id.tvClassbDelAnim);
        tvInoc = (TextView) findViewById(R.id.tvInocDelAnim);
        tvCubsq = (TextView) findViewById(R.id.tvCubsqDelAnim);

        //здесь создаётся база данных
        databaseHelper = new DatabaseHelper(getApplicationContext());
        db = databaseHelper.open();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getLong("id");
            System.out.println("Delete id: " + userId);
            tvID.setText(String.valueOf(userId));
        }

        if (userId > 0) {

            cursor = db.rawQuery("select * from " + DatabaseHelper.TABLE2 + " where "
                    + DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(userId)});

            System.out.println("USER ID : " + userId);
            cursor.moveToFirst();

//            System.out.println(cursor.getString(1));

            tvIdent.append(cursor.getString(1));
            tvGender.append(cursor.getString(2));
            tvAge.append(String.valueOf(cursor.getInt(3)));
            tvClassb.append(cursor.getString(4));
            tvInoc.append(cursor.getString(5));
            tvCubsq.append(String.valueOf(cursor.getInt(6)));
        }

    }
    public void onClickDelete(View view) {
        db.delete(DatabaseHelper.TABLE2, "_id = ?", new String[]{String.valueOf(userId)});
        goHome();
    }


    private void goHome() {
        long idd=-9;
        Intent intent = new Intent(this, SearchEditAnimActivity.class);
        intent.putExtra("id",idd);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);


    }


}

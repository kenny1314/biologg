package com.example.biologg.threeLvl;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.biologg.DatabaseHelper;
import com.example.biologg.R;

public class InfoAnimActivity extends AppCompatActivity {
    TextView tvID;
    TextView tvName;
    TextView tvGender;
    TextView tvAge;
    TextView tvClassb;
    TextView tvInoc;
    TextView tvCubsq;
    Cursor cursor;
    SQLiteDatabase db;
    DatabaseHelper databaseHelper;
    long userId = 0;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_anim);


        tvID = (TextView) findViewById(R.id.tvIDInfoAnim);
        tvName = (TextView) findViewById(R.id.tvNameInfoAnim);
        tvGender = (TextView) findViewById(R.id.tvGenderInfoAnim);
        tvAge = (TextView) findViewById(R.id.tvAgeInfoAnim);
        tvClassb = (TextView) findViewById(R.id.tvClassbInfoAnim);
        tvInoc = (TextView) findViewById(R.id.tvInocInfoAnim);
        tvCubsq = (TextView) findViewById(R.id.tvCubsqInfoAnim);


        //здесь создаётся база данных
        databaseHelper = new DatabaseHelper(getApplicationContext());
        db = databaseHelper.open();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getLong("id");
            System.out.println("Info id:" + userId);
            tvID.append(String.valueOf(userId));
        }

        if (userId > 0) {

            cursor = db.rawQuery("select * from " + DatabaseHelper.TABLE2 + " where "
                    + DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(userId)});

            System.out.println("USER ID : " + userId);
            cursor.moveToFirst();
            tvName.append(cursor.getString(1));
            tvGender.append(cursor.getString(2));
            tvAge.append(String.valueOf(cursor.getInt(3)));
            tvClassb.append(cursor.getString(4));
            tvInoc.append(cursor.getString(5));
            tvCubsq.append(String.valueOf(cursor.getInt(6)));
        }


    }
}

package com.example.biologg.threeLvl;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.biologg.DatabaseHelper;
import com.example.biologg.R;

public class InfoBioActivity extends AppCompatActivity {


    TextView tvID;
    TextView tvLogin;
    TextView tvIdent;
    TextView tvStage;
    TextView tvPassword;
    Cursor cursor;
    SQLiteDatabase db;
    DatabaseHelper databaseHelper;
    long userId = 0;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_bio);

        imageView = (ImageView) findViewById(R.id.imgBio);

        tvID = (TextView) findViewById(R.id.tvIDInfo);
        tvLogin = (TextView) findViewById(R.id.tvLoginInfoBio);
        tvIdent = (TextView) findViewById(R.id.tvIdentInfoBio);
        tvStage = (TextView) findViewById(R.id.tvStageInfoBio);
        tvPassword = (TextView) findViewById(R.id.tvPasswordInfoBio);


        //здесь создаётся база данных
        databaseHelper = new DatabaseHelper(getApplicationContext());
        db = databaseHelper.open();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getLong("id");
            System.out.println("Info id:" + userId);
            tvID.append(String.valueOf(userId));
        }


        int idimg = 0;
        if (userId > 0) {

            cursor = db.rawQuery("select * from " + DatabaseHelper.TABLE + " where "
                    + DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(userId)});

            System.out.println("USER ID : " + userId);
            cursor.moveToFirst();
            tvLogin.append(cursor.getString(1));
            tvIdent.append(cursor.getString(2));
            tvStage.append(String.valueOf(cursor.getInt(3)));
            tvPassword.append(cursor.getString(4));
            idimg = cursor.getInt(5);
        }


        //если idimg то вставить картинку
        String str1 = Long.toString(userId);


        System.out.println("___Idimg: " + idimg);

        if (idimg == 1) {
            imageView.setImageResource(R.drawable.b1);
        } else if (idimg == 2) {
            imageView.setImageResource(R.drawable.b2);
        } else if (idimg == 3) {
            imageView.setImageResource(R.drawable.b3);
        } else if (idimg == 4) {
            imageView.setImageResource(R.drawable.b4);
        } else {
            imageView.setImageResource(R.drawable.b2);
        }


    }

}

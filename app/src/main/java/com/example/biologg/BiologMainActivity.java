package com.example.biologg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.biologg.threeLvl.ChangeAnimActivity;
import com.example.biologg.threeLvl.ChangeBioActivity;
import com.example.biologg.twoLvl.SearchEditAnimActivity;
import com.example.biologg.twoLvl.SearchEditBioActivity;

public class BiologMainActivity extends AppCompatActivity {

    long userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biolog_main);


        //если 1, то биолог, если 2 то животные
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getLong("id");
            System.out.println("change " + userId);
        }

//        Toast toast = Toast.makeText(getApplicationContext(), "bio or anim: " + userId, Toast.LENGTH_SHORT);
//        toast.show();


    }

    public void onClickAddBio(View view) {
        Intent intent = null;

        if (userId == 1) {
            intent = new Intent(getApplicationContext(), ChangeBioActivity.class);
            intent.putExtra("id", 0);
            startActivity(intent);

        }

        if (userId == 2) {
            intent = new Intent(getApplicationContext(), ChangeAnimActivity.class);
            intent.putExtra("id", 0);
            startActivity(intent);
        }

    }

    public void onClickDelBio(View view) {


        if (userId == 1) {
            long idd = -9;
            Intent intent = new Intent(getApplicationContext(), SearchEditBioActivity.class);
            intent.putExtra("id", idd);
            startActivity(intent);

        }

        if (userId == 2) {
            long idd = -9;
            Intent intent = new Intent(getApplicationContext(), SearchEditAnimActivity.class);
            intent.putExtra("id", idd);
            startActivity(intent);
        }


    }


    public void onClickEditBio(View view) {

        if (userId == 1) {

            long idd = 1;
            Intent intent = new Intent(getApplicationContext(), SearchEditBioActivity.class);
            intent.putExtra("id", idd);
            startActivity(intent);
        }

        if (userId == 2) {
            long idd = 1;
            Intent intent = new Intent(getApplicationContext(), SearchEditAnimActivity.class);
            intent.putExtra("id", idd);
            startActivity(intent);
        }

    }


    public void onClickSearchBio(View view) {


        if (userId == 1) {
            long idd = 2;
            Intent intent = new Intent(getApplicationContext(), SearchEditBioActivity.class);
            intent.putExtra("id", idd);
            startActivity(intent);
        }

        if (userId == 2) {
            Intent intent = new Intent(getApplicationContext(), SearchEditAnimActivity.class);
            long idd = 2;
            intent.putExtra("id", idd);
            startActivity(intent);
        }


    }
}

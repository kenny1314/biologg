package com.example.biologg;


import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DatabaseHelper extends SQLiteOpenHelper {


    private static String DB_NAME = "cityinfo.db";

    private static String DB_PATH;


    public static final int SHEMA = 1;
    public static final String TABLE = "users";
    public static final String TABLE2 = "biodb";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_YEAR = "year";

    public static final String COLUMN_LOGINZ = "login";
    public static final String COLUMN_IDENTZ = "ident";
    public static final String COLUMN_STAGEZ = "stage";
    public static final String COLUMN_PASSWORDZ = "password";
    public static final String COLUMN_IDIMGZ = "idimg";



    public static final String COLUMN_IDENTB = "ident";
    public static final String COLUMN_GENDERB = "gender";
    public static final String COLUMN_AGEB = "age";
    public static final String COLUMN_CLASSB = "classb";
    public static final String COLUMN_INOC = "inoc";
    public static final String COLUMN_CUBSQ = "cubsq";
    private Context myContext;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, SHEMA);
        this.myContext = context;

        DB_PATH = Environment.getDataDirectory() + "/data/" + context.getPackageName() + "/databases/";

        System.out.println("Проверка на наличие базы данных:" + checkDataBase());
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public void create_db() {
        copyDataBase();
    }


    private void copyDataBase() {
        if (!checkDataBase()) {
            this.getReadableDatabase();
            this.close();
            try {
                copyDBFile();
            } catch (IOException mIOException) {
                throw new Error("ErrorCopyingDataBase ---- ошибка");
            }
        }
    }

    private void copyDBFile() throws IOException {
        InputStream mInput = myContext.getAssets().open(DB_NAME);
        OutputStream mOutput = new FileOutputStream(DB_PATH + DB_NAME);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0)
            mOutput.write(mBuffer, 0, mLength);

        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    public SQLiteDatabase open() throws SQLException {
        String myPath = DB_PATH + DB_NAME;
        return SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

}

package com.example.biologg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ChoseBioAnimActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chose_bio_anim);
    }


    public void onClickSwitchBio(View view) {
        Intent intent=new Intent(getApplicationContext(),BiologMainActivity.class);
        intent.putExtra("id",new Long(1));
        startActivity(intent);
    }

    public void onClickSwitchAnim(View view) {
        Intent intent=new Intent(getApplicationContext(),BiologMainActivity.class);
        intent.putExtra("id",new Long(2));
        startActivity(intent);
    }

}
